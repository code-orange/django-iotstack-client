import requests
import os


def iotstack_url():
    try:
        from django.conf import settings
    except ImportError:
        return os.getenv(
            "IOTSTACK_API_URL", default="https://iotstack-api.example.com/"
        )

    return settings.IOTSTACK_API_URL


def iotstack_default_credentials():
    try:
        from django.conf import settings
    except ImportError:
        username = os.getenv("IOTSTACK_API_USER", default="user")
        password = os.getenv("IOTSTACK_API_PASSWD", default="secret")
        return username, password

    username = settings.IOTSTACK_API_USER
    password = settings.IOTSTACK_API_PASSWD

    return username, password


def iotstack_head(endpoint: str, api_user: str, api_password: str, params=None):
    if params is None:
        params = dict()

    response = requests.head(
        f"{iotstack_url()}/{endpoint}",
        headers={
            "Authorization": "ApiKey {}:{}".format(
                api_user,
                api_password,
            ),
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        params=params,
    )

    return response.headers


def iotstack_get_data(endpoint: str, api_user: str, api_password: str, params=None):
    if params is None:
        params = dict()

    response = requests.get(
        f"{iotstack_url()}/{endpoint}",
        headers={
            "Authorization": "ApiKey {}:{}".format(
                api_user,
                api_password,
            ),
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        params=params,
    )

    return response.json()


def iotstack_post_data(
    endpoint: str, api_user: str, api_password: str, params=None, data=None
):
    if params is None:
        params = dict()

    if data is None:
        data = dict()

    response = requests.post(
        f"{iotstack_url()}/{endpoint}",
        headers={
            "Authorization": "ApiKey {}:{}".format(
                api_user,
                api_password,
            ),
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        params=params,
        json=data,
    )

    return response.json()


def iotstack_patch_data(
    endpoint: str, api_user: str, api_password: str, params=None, data=None
):
    if params is None:
        params = dict()

    if data is None:
        data = dict()

    response = requests.patch(
        f"{iotstack_url()}/{endpoint}",
        headers={
            "Authorization": "ApiKey {}:{}".format(
                api_user,
                api_password,
            ),
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        params=params,
        json=data,
    )

    return response.json()


def iotstack_put_data(
    endpoint: str, api_user: str, api_password: str, params=None, data=None
):
    if params is None:
        params = dict()

    if data is None:
        data = dict()

    response = requests.patch(
        f"{iotstack_url()}/{endpoint}",
        headers={
            "Authorization": "ApiKey {}:{}".format(
                api_user,
                api_password,
            ),
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        params=params,
        json=data,
    )

    return response.json()


def iotstack_delete_data(endpoint: str, api_user: str, api_password: str, params=None):
    if params is None:
        params = dict()

    response = requests.delete(
        f"{iotstack_url()}/{endpoint}",
        headers={
            "Authorization": "ApiKey {}:{}".format(
                api_user,
                api_password,
            ),
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        params=params,
    )

    if response.status_code == 200:
        return True

    return False


def iotstack_get_api_credentials_for_customer(mdat_id: int):
    username, password = iotstack_default_credentials()

    if username != mdat_id:
        response = iotstack_get_data(
            f"v1/apiauthcustomerapikey/{mdat_id}",
            username,
            password,
        )

        username = str(response["user"]["id"])
        password = response["key"]

    return username, password
